import Vue from 'vue'

import search from './component/search'
import modal from './component/modal'
import swich from './component/swich'
import axios from 'axios'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import { Modal,Button,Form, FormInput, FormGroup, Alert } from 'bootstrap-vue/es/components';
// Vue.use(Modal)
// Vue.use(Button)
// Vue.use(Form)
// Vue.use(FormInput)
// Vue.use(FormGroup)
// Vue.use(Alert)
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
// Vue.component('search', search)
Vue.prototype.$http = axios
/* eslint-disable no-new */
new Vue({
    el: '#app',
    data: {
        test: null
    },
    methods: {
        performSearch: function (e) { // Recherche en ajax par exemple }
            console.log(this.test)

            $('#addUser').modal('hide')
        }
    },
    delimiters: ['${', '}'],
    components:{
        search,
        modal,
        swich
    }
})