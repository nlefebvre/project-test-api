var Encore = require('@symfony/webpack-encore');
const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('css/app', './assets/css/app.css')
    .addStyleEntry('css/icons', './node_modules/@coreui/icons/css/coreui-icons.css')
    .addStyleEntry('css/font-awesome', './node_modules/font-awesome/css/font-awesome.css')
    .addStyleEntry('css/swich', ['./assets/scss/swich.scss'])
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())
    .cleanupOutputBeforeBuild()
    .enableVueLoader()
    .enableSassLoader()

// create hashed filenames (e.g. app.abc123.css)
// .enableVersioning()

// allow sass/scss files to be processed


;

// export the final configuration
const config = Encore.getWebpackConfig();
config.plugins.push( new VueLoaderPlugin())
module.exports = config;