<?php

namespace App\Backend\Repository;

use App\Entity\Films;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Films|null find($id, $lockMode = null, $lockVersion = null)
 * @method Films|null findOneBy(array $criteria, array $orderBy = null)
 * @method Films[]    findAll()
 * @method Films[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Films::class);
    }


    public function findBetteFilms():array
    {
        $qb =  $this->createQueryBuilder('f')
            ->select('COUNT(f.title) AS nbr_doublon, f.title')
            ->groupBy('f.title')
            ->orderBy('nbr_doublon','DESC');
        $qb ->andHaving($qb->expr()->gt($qb->expr()->count('f.title'),  1  ));
           $query = $qb->getQuery();

        return $query->getArrayResult();
        ;
    }
}
