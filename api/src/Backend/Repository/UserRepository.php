<?php

namespace App\Backend\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }
    /**
     * @param $id
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneToEdit($id)
    {

        $qb = $this->createQueryBuilder('u')

            ->where('u.id = :id')
            ->setParameter('id', $id);

        $query = $qb->getQuery();

        return $query->getOneOrNullResult();
    }
    public function findAlls()
    {
        return $this->createQueryBuilder('u')
            ->getQuery()
            ->getArrayResult();
        ;
    }
    public function filterSansFilms () {
        $qb = $this->createQueryBuilder('u');
        $qb->leftJoin('u.films','films');
        $qb->andWhere('films.users IS NULL');
        $query = $qb->getQuery();

        return $query->getResult();
        ;
    }

}
