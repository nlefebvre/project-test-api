<?php

namespace App\Backend\Controller;

use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class UserController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="User - Get all infos of the curruent user",
     *
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
     *     )
     * )
     */
    public function getUserAction()
    {
        $user = $this->get('cz.user')->findAll();

        $view = $this->view($user, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\RequestParam(
     *     name="pseudo",
     *     requirements="[a-zA-Z0-9]+",
     *     nullable=false,
     *     description="The name pesodo"
     * )
     * @Rest\RequestParam(
     *     name="email",
     *     requirements="^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,12}$",
     *     nullable=false,
     *     description="The email require"
     * )
     * @Rest\RequestParam(
     *     name="birthday",
     *     nullable=false,
     *     description="The birthday require"
     * )
     */
    public function postUserAction(ParamFetcherInterface $paramFetcher)
    {
        $user = $this->get('cz.user')->creatUser($paramFetcher);
        $view = $this->view($user, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\RequestParam(
     *     name="id",
     *     requirements="\d+",
     *     nullable=false,
     *     description="The name pesodo"
     * )
     * @Rest\RequestParam(
     *     name="title",
     *     nullable=false,
     *     description="The name pesodo"
     * )
     * @Rest\RequestParam(
     *     name="year",
     *     nullable=false,
     *     description="The name year"
     * )
     * @Rest\RequestParam(
     *     name="poster",
     *     nullable=false,
     *     description="The poster require"
     * )
     * @Rest\RequestParam(
     *     name="type",
     *     nullable=false,
     *     description="The language require"
     * )
     * @Rest\RequestParam(
     *     name="imdbID",
     *     nullable=false,
     *     description="The language require"
     * )
     */
    public function postUserFilmsAction(ParamFetcherInterface $paramFetcher)
    {
        $user = $this->get('cz.user')->addFilms($paramFetcher);
        $view = $this->view($user, 200);

        return $this->handleView($view);
    }
}
