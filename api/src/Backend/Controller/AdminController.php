<?php
/**
 * Created by PhpStorm.
 * User: nlefebvre
 * Date: 05/06/2018
 * Time: 14:23.
 */

namespace App\Backend\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * User: nlefebvre.
     * @Route("/admin", name="admin")
     */
    public function indexAction(Request $request)
    {
        $fitres = false;
        if ($request->query->has('filter')) {
            $user = $this->get('cz.user')->filterSansFilms();
            $fitres = $request->query->get('filter');
        } else {
            $user = $this->getDoctrine()->getRepository('App:User')->findAll();
        }
        $c = $this->getDoctrine()->getRepository('App:Films')->findBetteFilms();
        $countFims = null;
        if (!empty($c)) {
            $countFims = $c[0];
        }

        return $this->render('admin/index.html.twig', ['users' => $user, 'countFilms' => $countFims, 'fitres' => $fitres]);
    }

    /**
     * User: nlefebvre
     * @Route("/admin/add/{id}/films", name="admin_add")
     */
    public function addUserAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('App:User')->find($id);

        return $this->render('admin/addFilms.html.twig', ['users' => $user]);
    }

    /**
     * User: nlefebvre.
     * @Route("/admin/edit/{id}/films", name="admin_edit")
     */
    public function editUserAction(Request $request, $id)
    {
        $user = $this->get('cz.user')->findOneToEdit($id);

        return $this->render('admin/editFilms.html.twig', ['users' => $user, 'films' => $user->getFims()]);
    }

    /**
     * User: nlefebvre.
     * @Route("/admin/del/{id}/films/{idf}", name="admin_del")
     */
    public function deleteAction(Request $request, $id, $idf)
    {
        $user = $this->get('cz.user')->findOneToEdit($id);
        $flm = $this->getDoctrine()->getRepository('App\Entity\Films')->find($idf);

        $user->removeFilm($flm);

        $this->get('cz.user')->save($user);

        return $this->render('admin/editFilms.html.twig', ['users' => $user, 'films' => $user->getFims(), 'id' => $id]);
    }
}
