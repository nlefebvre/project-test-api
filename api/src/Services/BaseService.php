<?php

namespace App\Services;

use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\Common\Cache\FlushableCache;
use Doctrine\Common\Cache\MemcachedCache;
use Doctrine\ORM\EntityManager;

/**
 * Abstract class for the service which manage the Entity Manager
 */
abstract class BaseService
{
    /**
     * @var EntityManager The Entity Manager
     */
    protected $em;

    /**
     * Getter of the Entity Manager
     *
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Setter of the Entity Manager
     *
     * @param EntityManager $em the Entity Manager
     */
    public function setEntityManager($em)
    {
        $this->em = $em;
    }

    /**
     * Add a repository to this service
     *
     * @param integer $key   Key
     * @param string  $class Class
     *
     * @return void
     */
    public function addRepository($key, $class)
    {
        $this->$key = $this->em->getRepository($class);
    }

    /**
     * Add a service to this service
     *
     * @param integer $key     Key
     * @param string  $service Class
     *
     * @return void
     */
    public function addService($key, $service)
    {
        $this->$key = $service;
    }

    /**
     * Clear result cache
     *
     * @param string $prefix
     */
    public function clearResultCache($prefix = '')
    {
        $config = $this->em->getConfiguration();
        $cacheDriver = $config->getResultCacheImpl();
        $cacheDriver->flushAll();

        if (get_class($cacheDriver) == MemcachedCache::class) {
            $cacheDriver->doFlush();
        }
    }

}
