<?php

namespace App\Services;

use App\Entity\Films;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class UserService
 *
 * @package AppBundle\Services
 */
class UserService extends BaseService
{
    /**
     * @var Container
     */
    protected $container;
    /**
     * Repository
     *
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Manager
     *
     * @var UserManager Fos
     */
    protected $userManager;

    /**
     * Save a user
     *
     * @param User $user
     *
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(User $user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    /**
     * Remove one user
     *
     * @param User $user
     *
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(User $user)
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }

    /**
     * find All
     *
     * @return mixed
     */
    public function findAll()
    {

//        return $this->userRepository->findBy([], ['createdAt' => 'DESC']);
        return $this->userRepository->findAlls();
    }

    /**
     * filterSansFilms
     *
     * @return mixed
     */
    public function filterSansFilms()
    {

        return $this->userRepository->filterSansFilms();
    }

    /**
     * Find user by slug for edit profil
     *
     * @param string $id
     *
     * @return User
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneToEdit($id)
    {
        return $this->userRepository->findOneToEdit($id);
    }
    public function findUserByEmail($email){
        return $this->userRepository->findByEmail($email) ? true : false;
    }

    public function addFilms(ParamFetcherInterface $paramFetcher){
       $user = $this->userRepository->findOneToEdit($paramFetcher->get('id'));
       $fims = new Films();


       $fims->setTitle($paramFetcher->get('title'));
       $fims->setPoster($paramFetcher->get('poster'));
        $fims->setType($paramFetcher->get('type'));
        $fims->setYear($paramFetcher->get('year'));
        $fims->setImdbID($paramFetcher->get('imdbID'));

        $user->addFilm($fims);

        $this->save($user);
        return $user;
    }
    public function creatUser(ParamFetcherInterface $paramFetcher){
        $user = new User();

        if (empty($paramFetcher->get('email')) || $this->findUserByEmail($paramFetcher->get('email'))) {
            throw new BadRequestHttpException("This e-mail already exist.");
        }

        $user->setPseudo($paramFetcher->get('pseudo'))->setBirthdate(new \DateTime($paramFetcher->get('birthday')))->setEmail($paramFetcher->get('email'));
        $this->save($user);
        return $user;
    }
    /**
     * Generate password
     *
     * @return mixed
     */
    public function generatePassword()
    {
        $chars = 'abcdefghkmnpqrstuvwxyz';
        $numbers = '123456789';
        $password = substr($chars, rand(0, strlen($chars) - 1), 1) . substr($chars, rand(0, strlen($chars) - 1), 1) . substr($chars, rand(0, strlen($chars) - 1), 1) . substr($chars, rand(0, strlen($chars) - 1), 1);
        $password .= substr($numbers, rand(0, strlen($numbers) - 1), 1) . substr($numbers, rand(0, strlen($numbers) - 1), 1) . substr($numbers, rand(0, strlen($numbers) - 1), 1) . substr($numbers, rand(0, strlen($numbers) - 1), 1);

        return str_shuffle($password);
    }
}
